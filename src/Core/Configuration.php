<?php namespace Rootcore\Core;

class Configuration {
    /**
     * El idioma de la aplicacion.
     *
     * @var string
     */
    protected $lang;

    /**
     * La zona horaria.
     *
     * @var string
     */
    protected $timezone;

    /**
     * Las rutas de la aplicacion
     *
     * @var array
     */
    public $paths;

    /**
     * Establece el idioma de la aplicacion.
     *
     * @param string $imei The username string to set
     * @return void
     * @version 1.0
     * @since 10-01-2016 19:03:00
     * @author Yesid Durán G. (Yeligoth) <yesiddg2014@gmail.com>
     */
    public function setLang($lang) {
        $this->lang = $lang;
    }

    /**
     * Establece la zona horaria de la aplicación.
     *
     * @param string $imei The username string to set
     * @return void
     * @version 1.0
     * @since 10-01-2016 9:07:00
     * @author Yesid Durán G. (Yeligoth) <yesiddg2014@gmail.com>
     */
    public function setTimezone($timezone) {
        $this->timezone = $timezone;
    }

    /**
     * Establece las rutas principales de la aplicación.
     *
     * @param string $imei The username string to set
     * @return void
     * @version 1.0
     * @since 10-01-2016 9:07:00
     * @author Yesid Durán G. (Yeligoth) <yesiddg2014@gmail.com>
     */
    public function setPaths(array $paths) {
        $this->paths = $paths;
    }
}
<?php namespace Rootcore\Core;

use Rootcore\Core\Configuration;
use RuntimeException;

class Application {
    /**
     * El entorno personalizado definido por el desarrollador.
     *
     * @var string
     */
    protected $environment;

    /**
     * La configuración de la aplicación.
     *
     * @var \Rootcore\Core\Configuration
     */
    protected $config;

    /**
     * La ruta base para la instalación de la aplicacion.
     *
     * @var string
     */
    protected $basePath;

    /**
     * El archivo de entorno cargado durante el bootstrapping.
     *
     * @var string
     */
    protected $environmentFile = '.env';

    /**
     * El namespace de la aplicación
     *
     * @var string
     */
    protected $namespace = null;

    /**
     * Creación e inicialización de las propiedades de una nueva instancia de application
     *
     * @param \Rootcore\Core\Configuration $config
     * @return void
     * @version 1.0
     * @since 2016-01-10 20:15:00
     * @author Yesid Durán G. (Yeligoth) <yesiddg2014@gmail.com>
     */
    public function __construct(array $config, $environment, $basePath) {
        $this->environment = $environment;
        $this->setConfig($config);

        if ($basePath) {
            $this->setBasePath($basePath);
        }
    }

    protected function setConfig($config) {
        $this->config = $config;

        /**
         * --------------------------- Establecer Opciones de Notificacion de Errores -----------------------------
         * Establecemos la directiva de notificacion de errores, todos los tipos de errores van hacer notificados.
         */
        error_reporting(-1);

        /**
         * --------------------------------------- Establecer la Zona Horaria -------------------------------------
         * Aqui establecemos la zona horaria utilizada en la aplicacion para afectar a las funciones de hora y fecha
         * El valor sera escogido desde las opciones de configuracion, establecidas por el usuario en el archivo
         * application/config/app.php
         */
        date_default_timezone_set($this->config['timezone']);

        /*
         * ----------------------------------- Despligue de errores ------------------------------------
         * Habilita o deshabilita el despliegue de errores.
         */
        ini_set('display_errors', $this->config['debug']);

        /**
         * -------------------------------------Despliegue de errores de inicio ----------------------------------
         * Habilita o deshabilita el despliegue de errores durante la secuencia de arranque de php.
         */
        ini_set('display_startup_errors', $this->config['debug']);
    }

    /**
     * Establece la ruta base para la aplicación
     *
     * @param string $basePath
     * @return void
     * @version 1.0
     * @since 2016-03-16 21:56:00
     * @author Yesid Durán G. (Yeligoth) <yesiddg2014@gmail.com>
     */
    public function setBasePath($basePath) {
        $this->basePath = rtrim($basePath, '\/');
    }

    /**
     * Obtiene el entorno de ejecución actual.
     *
     * @return string
     * @version 1.0
     * @since 2016-03-16 20:03:00
     * @author Yesid Durán G. (Yeligoth) <yesiddg2014@gmail.com>
     */
    public function getEnvironment() {
        return $this->environment;
    }

    /**
     * Obtiene la ruta base de la aplicación.
     *
     * @return string
     * @version 1.0
     * @since 2016-03-16 20:20:00
     * @author Yesid Durán G. (Yeligoth) <yesiddg2014@gmail.com>
     */
    public function getBasePath() {
        return $this->basePath;
    }

    /**
     * Obtiene el namespace de la aplicación
     *
     * @return string
     * @throws \RuntimeException
     * @version 1.0
     * @since 2016-03-16 21:40:00
     * @author Yesid Durán G. (Yeligoth) <yesiddg2014@gmail.com>
     */
    public function getNamespace()
    {
        if (! is_null($this->namespace)) {
            return $this->namespace;
        }

        $composer = json_decode(file_get_contents($this->getBasePath().DIRECTORY_SEPARATOR.'composer.json'), true);

        print_r($composer);
        /*foreach ((array) data_get($composer, 'autoload.psr-4') as $namespace => $path) {
            foreach ((array) $path as $pathChoice) {
                if (realpath(app_path()) == realpath($this->getBasePath().'/'.$pathChoice)) {
                    return $this->namespace = $namespace;
                }
            }
        }*/

        //throw new RuntimeException('Unable to detect application namespace.');
    }
}